﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("Fun.FontDecode")]
[assembly: AssemblyDescription("Fun FontDecode Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NAILY Soft")]
[assembly: AssemblyProduct("Fun.FontDecode")]
[assembly: AssemblyCopyright("Copyright ©  2020. NAILY Soft, ALL RIGHTS RESERVED.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//将 ComVisible 设置为 false 将使此程序集中的类型
//对 COM 组件不可见。  如果需要从 COM 访问此程序集中的类型，
//请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("765a1f20-cc95-4efb-890f-9b938e53bb7a")]

// 程序集的版本信息由下列四个值组成: 
//
//      主版本
//      次版本
//      生成号
//      修订号
//
//可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值，
// 方法是按如下所示使用“*”: :
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2001.1")]
[assembly: AssemblyFileVersion("1.0.2001.1")]
