﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fun.FontDecode
{
    public interface IFontDecoder
    {
        /// <summary>
        /// 解密字符串。
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        string Decode(string s);
    }
}
