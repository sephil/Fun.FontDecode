﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fun.FontDecode.OpenTypes
{
    internal static class BEHelper
    {        
        public static UInt16 Reverse(this UInt16 value)
        {
            return (UInt16)((value & 0xFFU) << 8 | (value & 0xFF00U) >> 8);
        }

        public static Int16 Reverse(this Int16 value)
        {
            return (Int16)Reverse((UInt16)value);
        }

        public static UInt32 Reverse(this UInt32 value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
                   (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }

        public static Int32 Reverse(this Int32 value)
        {
            return (Int32)Reverse((UInt32)value);
        }

        public static UInt64 Reverse(this UInt64 value)
        {
            return (value & 0x00000000000000FFUL) << 56 | (value & 0x000000000000FF00UL) << 40 |
                   (value & 0x0000000000FF0000UL) << 24 | (value & 0x00000000FF000000UL) << 8 |
                   (value & 0x000000FF00000000UL) >> 8 | (value & 0x0000FF0000000000UL) >> 24 |
                   (value & 0x00FF000000000000UL) >> 40 | (value & 0xFF00000000000000UL) >> 56;
        }

        public static Int64 Reverse(this Int64 value)
        {
            return (Int64)Reverse((UInt64)value);
        }

        public static byte[] Reverse(this byte[] value)
        {
            byte tmp;
            int len = value.Length;

            for (int i = 0; i < len / 2; i++)
            {
                tmp = value[len - 1 - i];
                value[len - 1 - i] = value[i];
                value[i] = tmp;
            }

            return value;
        }
    }
}
