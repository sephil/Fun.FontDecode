﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fun.FontDecode.OpenTypes
{
    public abstract class ParserBase
    {
        public void ParseFile(string filename)
        {
            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Parse(stream);
            }
        }

        public void Parse(Stream stream)
        {
            using (var reader = new BinaryReader(stream))
            {
                Parse(reader);
            }
        }

        public void Parse(byte[] data)
        {
            using (var stream = new MemoryStream(data, false))
            {
                Parse(stream);
            }
        }

        public void Parse(string base64)
        {
            Parse(Convert.FromBase64String(base64));
        }

        public abstract void Parse(BinaryReader reader);
    }
}
