﻿namespace Fun.FontDecode.Viewer
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReloadBtn = new System.Windows.Forms.Button();
            this.LMapEdit = new System.Windows.Forms.TextBox();
            this.RMapEdit = new System.Windows.Forms.TextBox();
            this.CopySBtn = new System.Windows.Forms.Button();
            this.FileNameEdit = new System.Windows.Forms.ComboBox();
            this.FileBtn = new System.Windows.Forms.Button();
            this.FolderBtn = new System.Windows.Forms.Button();
            this.CopyDBtn = new System.Windows.Forms.Button();
            this.PaintPanel = new System.Windows.Forms.Panel();
            this.GridScroll = new System.Windows.Forms.VScrollBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PaintPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReloadBtn
            // 
            this.ReloadBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReloadBtn.Location = new System.Drawing.Point(603, 12);
            this.ReloadBtn.Name = "ReloadBtn";
            this.ReloadBtn.Size = new System.Drawing.Size(85, 31);
            this.ReloadBtn.TabIndex = 3;
            this.ReloadBtn.Text = "&Reload";
            this.ReloadBtn.UseVisualStyleBackColor = true;
            this.ReloadBtn.Click += new System.EventHandler(this.ReloadBtn_Click);
            // 
            // LMapEdit
            // 
            this.LMapEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LMapEdit.Location = new System.Drawing.Point(12, 59);
            this.LMapEdit.Multiline = true;
            this.LMapEdit.Name = "LMapEdit";
            this.LMapEdit.ReadOnly = true;
            this.LMapEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LMapEdit.Size = new System.Drawing.Size(676, 270);
            this.LMapEdit.TabIndex = 6;
            this.LMapEdit.WordWrap = false;
            // 
            // RMapEdit
            // 
            this.RMapEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RMapEdit.Location = new System.Drawing.Point(694, 59);
            this.RMapEdit.Multiline = true;
            this.RMapEdit.Name = "RMapEdit";
            this.RMapEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RMapEdit.Size = new System.Drawing.Size(241, 270);
            this.RMapEdit.TabIndex = 7;
            this.RMapEdit.WordWrap = false;
            // 
            // CopySBtn
            // 
            this.CopySBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopySBtn.Location = new System.Drawing.Point(694, 12);
            this.CopySBtn.Name = "CopySBtn";
            this.CopySBtn.Size = new System.Drawing.Size(118, 31);
            this.CopySBtn.TabIndex = 4;
            this.CopySBtn.Text = "Copy &Static";
            this.CopySBtn.UseVisualStyleBackColor = true;
            this.CopySBtn.Click += new System.EventHandler(this.CopySBtn_Click);
            // 
            // FileNameEdit
            // 
            this.FileNameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileNameEdit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FileNameEdit.FormattingEnabled = true;
            this.FileNameEdit.Location = new System.Drawing.Point(206, 16);
            this.FileNameEdit.Name = "FileNameEdit";
            this.FileNameEdit.Size = new System.Drawing.Size(391, 23);
            this.FileNameEdit.TabIndex = 2;
            this.FileNameEdit.SelectedIndexChanged += new System.EventHandler(this.ReloadBtn_Click);
            // 
            // FileBtn
            // 
            this.FileBtn.Location = new System.Drawing.Point(12, 12);
            this.FileBtn.Name = "FileBtn";
            this.FileBtn.Size = new System.Drawing.Size(91, 31);
            this.FileBtn.TabIndex = 0;
            this.FileBtn.Text = "&File...";
            this.FileBtn.UseVisualStyleBackColor = true;
            this.FileBtn.Click += new System.EventHandler(this.FileBtn_Click);
            // 
            // FolderBtn
            // 
            this.FolderBtn.Location = new System.Drawing.Point(109, 12);
            this.FolderBtn.Name = "FolderBtn";
            this.FolderBtn.Size = new System.Drawing.Size(91, 31);
            this.FolderBtn.TabIndex = 1;
            this.FolderBtn.Text = "F&older...";
            this.FolderBtn.UseVisualStyleBackColor = true;
            this.FolderBtn.Click += new System.EventHandler(this.FolderBtn_Click);
            // 
            // CopyDBtn
            // 
            this.CopyDBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopyDBtn.Location = new System.Drawing.Point(818, 12);
            this.CopyDBtn.Name = "CopyDBtn";
            this.CopyDBtn.Size = new System.Drawing.Size(118, 31);
            this.CopyDBtn.TabIndex = 5;
            this.CopyDBtn.Text = "Copy &Dynamic";
            this.CopyDBtn.UseVisualStyleBackColor = true;
            this.CopyDBtn.Click += new System.EventHandler(this.CopyDBtn_Click);
            // 
            // PaintPanel
            // 
            this.PaintPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PaintPanel.Controls.Add(this.GridScroll);
            this.PaintPanel.Location = new System.Drawing.Point(12, 346);
            this.PaintPanel.Name = "PaintPanel";
            this.PaintPanel.Size = new System.Drawing.Size(924, 285);
            this.PaintPanel.TabIndex = 8;
            this.PaintPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintPanel_Paint);
            this.PaintPanel.Resize += new System.EventHandler(this.PaintPanel_Resize);
            // 
            // GridScroll
            // 
            this.GridScroll.Dock = System.Windows.Forms.DockStyle.Right;
            this.GridScroll.Location = new System.Drawing.Point(903, 0);
            this.GridScroll.Name = "GridScroll";
            this.GridScroll.Size = new System.Drawing.Size(21, 285);
            this.GridScroll.TabIndex = 0;
            this.GridScroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.GridScroll_Scroll);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 643);
            this.Controls.Add(this.PaintPanel);
            this.Controls.Add(this.CopyDBtn);
            this.Controls.Add(this.FolderBtn);
            this.Controls.Add(this.FileBtn);
            this.Controls.Add(this.FileNameEdit);
            this.Controls.Add(this.CopySBtn);
            this.Controls.Add(this.RMapEdit);
            this.Controls.Add(this.LMapEdit);
            this.Controls.Add(this.ReloadBtn);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Font Decode & Viewer";
            this.PaintPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ReloadBtn;
        private System.Windows.Forms.TextBox LMapEdit;
        private System.Windows.Forms.TextBox RMapEdit;
        private System.Windows.Forms.Button CopySBtn;
        private System.Windows.Forms.ComboBox FileNameEdit;
        private System.Windows.Forms.Button FileBtn;
        private System.Windows.Forms.Button FolderBtn;
        private System.Windows.Forms.Button CopyDBtn;
        private System.Windows.Forms.Panel PaintPanel;
        private System.Windows.Forms.VScrollBar GridScroll;
        private System.Windows.Forms.Timer timer1;
    }
}

